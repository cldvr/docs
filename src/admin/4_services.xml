<?xml version="1.0" encoding="utf-8"?>
<chapter label="4" id="services">
    <title>Service management</title>
    <highlights><para>In this chapter, we will discuss service management in the Adélie Linux system.  This will include long-running processes (or <firstterm>service</firstterm>s), scheduling processes to run at a later time, and scheduling processes to run periodically.</para></highlights>
    <note><title>Note</title><para>For the service management section of this chapter, an OpenRC installation is assumed.  Other process management systems, including the s6 supervision suite, are not yet discussed in this documentation.</para></note>
    <section id="svc_mgmt_rc">
        <title>Services</title>
        <para>A <firstterm>service</firstterm>, also called a <firstterm>daemon</firstterm> or <firstterm>server process</firstterm>, is a long-running process that provides a function for your computer, or for others on your network.  A service may be a web server, print spooler, a screen resource manager, or any other useful process.  There are a number of high-quality services packaged and available for you to install on your Adélie Linux system.  Once you install one (or more), you will need to configure and enable them.  You can then start, restart, stop, or view the status of any service on your system.  In this section, we will use the <package>OpenSSH</package> remote shell service in our examples.  OpenSSH allows you to remotely connect to your computer from another computer using your login name and password, and run commands on it from another computer.</para>
        <section>
            <title>Installing a service</title>
            <para>Refer to <xref linkend="packages"/> for detailed information about installing packages.  When you install the <package>openssh</package> package using <command>apk add openssh</command>, you may notice that APK automatically installs the <package>openssh-openrc</package> package for you.  Services packaged by Adélie Linux always include -openrc subpackages which allow you to quickly and easily bring up new services on your systems.</para>
            <para>All available services are in the <filename class="directory">/etc/init.d</filename> directory; you may list the contents of this directory to determine what services are available on your computer.</para>
        </section>
        <section id="enable_disable_rc">
            <title>Enabling and disabling services</title>
            <warning><title>Security Notice</title><para>Remember to always review a service's documentation and best practices for configuration before enabling and starting the service.</para></warning>
            <para>To enable a service, you may use the command <command>rc-update add <userinput>SERVICE</userinput> default</command>, where <userinput>SERVICE</userinput> is the name of the service &mdash; in this example, <literal>sshd</literal>.  This adds the service to OpenRC's <firstterm>default runlevel</firstterm>, which means the service will automatically start when the system is booted.  You may configure other runlevels based on your needs; however, this process is beyond the scope of this handbook.  Refer to <ulink url="https://wiki.gentoo.org/wiki/Handbook:AMD64/Working/Initscripts">the official OpenRC documentation</ulink> for more information.</para>
            <para>To disable a service, and prevent it from automatically starting on system boot, you may use the command <command>rc-update del <userinput>SERVICE</userinput> default</command>, where <userinput>SERVICE</userinput> is the name of the service.</para>
        </section>
        <section id="start_stop_rc">
            <title>Starting, stopping, and restarting services</title>
            <para>To start a service, you may use the command <command>service <userinput>SERVICE</userinput> start</command>, where <userinput>SERVICE</userinput> is the name of the service you wish to start.  To stop a service, you may use the command <command>service <userinput>SERVICE</userinput> stop</command>, and to restart, you may use the command <command>service <userinput>SERVICE</userinput> restart</command>.</para>
        </section>
        <section>
            <title>Viewing service status</title>
            <para>To view the status of a single service, you may use the command <command>service <userinput>SERVICE</userinput> status</command>, where <userinput>SERVICE</userinput> is the service for which you wish to view status.  This command will output <literal>started</literal> if a service is currently running, <literal>stopped</literal> if a service is not currently running, and <literal>crashed</literal> if a service stopped running prematurely.</para>
            <para>To view the status of all services on your system, you may use the command <command>rc-status</command>.  If any services are listed under "Dynamic Runlevel: manual", these are services that have been started but do not belong to any runlevel.  That means they have been started without being enabled first, and will not automatically start on next system boot.</para>
        </section>
    </section>
    <section id="at">
        <title>Scheduling a process to run later</title>
        <warning><title>Not Yet Written</title><para>This section has not yet been written.</para></warning>
    </section>
    <section id="cron">
        <title>Scheduling a process to run periodically</title>
        <warning><title>Not Yet Written</title><para>This section has not yet been written.</para></warning>
    </section>
</chapter>
