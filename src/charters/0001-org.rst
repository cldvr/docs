===============================================
 Project Organisation Charter for Adélie Linux
===============================================
:Authors:
  * **A. Wilcox**
:Status:
  Draft; RFC
:Copyright:
  © 2019 Adélie Linux Team.  CC BY-NC-ND-SA 4.0.



Introduction
============

Adélie Linux is growing and maturing into a full-featured Linux distribution
with many individual contributors, goals, and directions of growth.  The
Platform Group, serving as the core contributors of the project, desire an
organisational structure that serves the needs of the community.  This
structure shall ensure that people who are new to the community can easily
contribute to the project.  This structure shall additionally ensure that our
community's goals can be accomplished swiftly and with proper documentation. 

Adding too much formality to this organisational structure could cause undue
delays and pressure on the community, and could stifle innovation and
creativity.  Therefore, most of the structure that this charter proposes is
rooted in the processes that Adélie has already organically grown.  We feel
that defining these processes, and refining them into even better processes, is
the best way forward for our community.

For the purposes of this document, a Committer is a contributor to Adélie Linux
whom has write access to at least one official Adélie Linux Git repository.
The process in which Committers are inaugerated into Adélie Linux is described
in a separate Charter.



Projects
========

Our first act in this charter is to define "Projects".  A Project is a
collective of contributors working towards a common goal.  This goal should be
on-going and clearly defined (see Interest Groups below for one-time tasks).
At least one Committer is required to form a Project.

All Projects will be assigned a mailing list address, typically adelie-$project
where $project is a unique, short name for the Project.

Projects will be granted a Web space for any documentation they feel
appropriate.  All Projects will be required to maintain a list of Committers
involved in the Project on this Web space.  Projects may additionally maintain
a list of frequent non-Committer contributors as well, but they are not
required to do so.

A Project that relates to packages will have an official designated branch of
packages.git.  Non-Committer contributors will create merge requests to this
branch, to be reviewed by a Committer in the Project.  Committers in the
Project may either create merge requests to this branch, or commit directly to
this branch.  A Project may be listed as the maintainer of any package that is
relevant to it.

Periodically, it is expected that the Project will want to merge its changes in
to the main Adélie Linux system.  This will be accomplished by the Project
opening a merge request to the `master` branch, which will be reviewed by at
least one member of the Platform Group.


Creating a Project
------------------

A Project may be proposed for creation at any time by a Committer, via sending
an email to the adelie-project@ mailing list.  This proposal must be approved
by a simple majority of the Platform Group.

The Project proposal must contain the name of the project, the desired short
name for the mailing list, and the common goal it wishes to achieve.

The proposal for a new Project may contain an initial list of members.  If so,
each member is expected to respond to the message on the adelie-project@
mailing list with an ACK.  If they do not respond to the message within 48
hours, they will not be added to the initial roster of members of the Project.


Joining and Leaving a Project
-----------------------------

Projects may describe their own processes for joining and leaving their
respective Project.

The default process, if a Project does not describe their own, is that any
Committer in a Project may nominate any member of the community to join the
Project.  The community member then ACKs this using the Project's preferred
communication medium; either mailing list or IRC channel.  If no other
Committer is present in the Project, the member is then added; otherwise, at
least one other Committer must agree.

A community member may leave a Project for any reason.  They must communicate
this desire to a Committer in the Project.


Disbanding a Project
--------------------

A Project may be disbanded without Platform Group interference if all
Committers assigned to the Project wish it so.

The Platform Group may disband a project with a three-fourths majority.

When a Project is disbanded, its mailing list must be made read-only, and its
archives must be retained.  The Platform Group may destroy the Project's Web
space only under extenuating circumstances.  In the event of destruction, URLs
must be set to return a '410 Gone' response.



Interest Groups
===============

Our second act in this charter is to define "Interest Groups".  An Interest
Group is a collective of contributors working to complete a single task.  This
task should be clearly defined, with a reasonable expectation of completion.

An Interest Group is more informal than a Project; it will only be assigned a
mailing list if a need for one is demonstrated.  Additionally, Interest Groups
related to a package or set of packages will not have an official branch of
packages.git created.  It is expected that Interest Groups will work with any
relevant Project(s) to accomplish their packaging tasks.

Interest Groups will be granted Web space for any documentation they feel
appropriate.  Interest Groups, like Projects, shall maintain a list of
affiliated Committers on this Web space.  They may, additionally, list
non-Committer regular contributors, though this is not required.



Platform Group
==============

This charter, as with most of the Adélie Linux system, depends heavily on a
functional Platform Group, which acts as the "root" of the structure.  As such,
a formal declaration of the Platform Group's responsibilities is provided in
this section.  Additionally, addition and removal of members of the Platform
Group are discussed.

No current member of ComArb may be a concurrent member of the Platform Group.
Similarly, a member of the Platform Group may not be a concurrent member of
ComArb.

The Project Lead is additionally the lead of the Platform Group.


Responsibilities
----------------

The Platform Group maintains the base system packages of Adélie Linux.  This
includes the origin of the Platform Group's name: the kernel and toolchain.

The Platform Group ensures orderly and efficient processes, including those
revolving around Projects and Interest Groups.

The Platform Group handles addition and removal of Committers.

The Platform Group determines which CPU architectures will be supported by the
Adélie Linux distribution, and in which tier.

The Platform Group, being the base project of Adélie Linux, may not be
dissolved under any circumstance.  In the event that no members are left in the
Platform Group, all Committers must vote to elect a new Project Lead.  The new
Project Lead must win via simple majority of all Committers.


Addition
--------

Nomination of a new member of the Platform Group may be made by any Committer.
The new member must be an existing Committer with no disciplinary action taken
against them in the past six months.  The existing Platform Group will then 
hold a vote on the candidate; a simple majority will allow the addition of the
Committer to the Platform Group.

If the Platform Group does not approve the addition of the Committer to the
Platform Group, they may be overridden by a three-fourths majority of
Committers.


Removal
-------

Expulsion of a member of the Platform Group may be initiated by either a member
of the Platform Group itself, or a member of Community Arbitration.

A vote on whether to expel the member must be held at least 48 hours after the
expulsion announcement, to avoid emotionally charged voting.  During this time,
the member may make a statement if they desire to do so.

All members of the Platform Group vote except the member being expelled.  A
three-fourths majority of members must approve the expulsion.  If less than
four members of the Platform Group remain to vote, then the largest possible
supermajority is required to approve the expulsion.

If the Platform Group does not approve the expulsion, it may be overridden by a
complete approval of the expulsion by every Committer.


Voting
------

In the event of a Platform Group vote resulting in a tie, the vote of the
current Project Lead will be carried.  If the Project Lead is absent, the vote
of the senior-most member of the Platform Group that is present for voting will
be carried.

Seniority in the Platform Group is based on the time since addition to the
Platform Group; it is not based on total time contributing to the project.
