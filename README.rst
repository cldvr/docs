=======================================
 README for Adélie Linux Documentation
=======================================
:Authors:
 * **A. Wilcox**, primary writer
 * **Adélie Linux users and contributors**, additional documentation
:Status:
 Draft
:Copyright:
 © 2015-2018 Adélie Linux.  CC BY-NC-SA open source license.



Introduction
============

This distribution contains the documentation set for Adélie Linux.


License
```````
This documentation is licensed under a
Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by-nc-sa/4.0/>.


Changes
```````
Any changes to this repository must be reviewed before being pushed to the
master branch.
